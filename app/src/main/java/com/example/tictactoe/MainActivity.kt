package com.example.tictactoe

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var isFirstPlayer = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        tryAgainButton.setOnClickListener {
            isFirstPlayer=true
            button00.isClickable = true
            button00.text = ""
            button01.isClickable = true
            button01.text = ""
            button02.isClickable = true
            button02.text = ""
            button10.isClickable = true
            button10.text = ""
            button11.isClickable = true
            button11.text = ""
            button12.isClickable = true
            button12.text = ""
            button20.isClickable = true
            button20.text = ""
            button21.isClickable = true
            button21.text = ""
            button22.isClickable = true
            button22.text = ""
        }


    }

    private fun init() {
        button00.setOnClickListener {
            buttonChanger(button00)

        }
        button01.setOnClickListener {
            buttonChanger(button01)

        }
        button02.setOnClickListener {
            buttonChanger(button02)

        }
        button10.setOnClickListener {
            buttonChanger(button10)

        }
        button11.setOnClickListener {
            buttonChanger(button11)

        }
        button12.setOnClickListener {
            buttonChanger(button12)

        }
        button20.setOnClickListener {
            buttonChanger(button20)

        }
        button21.setOnClickListener {
            buttonChanger(button21)

        }
        button22.setOnClickListener {
            buttonChanger(button22)

        }
            


    }

    private fun buttonChanger(button: Button) {
        if (isFirstPlayer)
            button.text = "X"
        else
            button.text = "0"
        button.isClickable = false
        isFirstPlayer = !isFirstPlayer
        checkWinner()

    }

    private fun checkWinner() {
        if (button00.text.toString().isNotEmpty() && button00.text.toString() == button01.text.toString() && button01.text.toString() == button02.text.toString()) {
            showToast(button00.text.toString())
            notClickableButton()
        } else if (button10.text.toString().isNotEmpty() && button10.text.toString() == button11.text.toString() && button11.text.toString() == button12.text.toString()) {
            showToast(button10.text.toString())
            notClickableButton()
        } else if (button20.text.toString().isNotEmpty() && button20.text.toString() == button21.text.toString() && button21.text.toString() == button22.text.toString()) {
            showToast(button20.text.toString())
            notClickableButton()
        } else if (button00.text.toString().isNotEmpty() && button00.text.toString() == button10.text.toString() && button10.text.toString() == button20.text.toString()) {
            showToast(button00.text.toString())
            notClickableButton()
        } else if (button01.text.toString().isNotEmpty() && button01.text.toString() == button11.text.toString() && button11.text.toString() == button21.text.toString()) {
            showToast(button01.text.toString())
            notClickableButton()
        } else if (button02.text.toString().isNotEmpty() && button02.text.toString() == button12.text.toString() && button12.text.toString() == button22.text.toString()) {
            showToast(button02.text.toString())
            notClickableButton()
        } else if (button00.text.toString().isNotEmpty() && button00.text.toString() == button11.text.toString() && button11.text.toString() == button22.text.toString()) {
            showToast(button00.text.toString())
            notClickableButton()
        } else if (button02.text.toString().isNotEmpty() && button02.text.toString() == button11.text.toString() && button11.text.toString() == button20.text.toString()) {
            showToast(button02.text.toString())
            notClickableButton()
        } else if (button00.text.toString().isNotEmpty() && button01.text.toString().isNotEmpty() && button02.text.toString().isNotEmpty() && button10.text.toString().isNotEmpty() && button11.text.toString().isNotEmpty()&& button12.text.toString().isNotEmpty() && button20.text.toString().isNotEmpty() && button21.text.toString().isNotEmpty() && button22.text.toString().isNotEmpty()){
            makeText(this, "Tie", LENGTH_SHORT).show()
        }
    }


    private fun showToast(message: String) =
            makeText(this, "Winner is $message", LENGTH_SHORT).show()




    private fun notClickableButton(){
        button00.isClickable =false
        button01.isClickable =false
        button02.isClickable =false
        button10.isClickable =false
        button11.isClickable =false
        button12.isClickable =false
        button20.isClickable =false
        button21.isClickable =false
        button22.isClickable =false
    }
        }




